PATH_DIR=.
API_EXEC=docker-compose exec api

build:
	docker-compose build

runserver:
	docker-compose up -d 

stopserver:
	docker-compose stop api && docker-compose rm api

test:
	docker-compose exec api python manage.py test $(PATH_DIR)

makemigrations:
	docker-compose exec api python manage.py makemigrations

migrate:
	docker-compose exec api python manage.py migrate

shell:
	docker-compose exec api python manage.py shell

logs:
	docker-compose logs

test_coverage:
	$(API_EXEC) coverage run --source='.' manage.py test

report:
	$(API_EXEC) coverage report

report_html:
	$(API_EXEC) coverage html

flake8:
	$(API_EXEC) flake8 $(PATH_DIR)

black:
	$(API_EXEC) black $(PATH_DIR)

black_check:
	$(API_EXEC) black $(PATH_DIR) --check

black_diff:
	$(API_EXEC) black $(PATH_DIR) --diff

isort:
	$(API_EXEC) isort .

isort_check:
	$(API_EXEC) isort . --check-only

down:
	docker-compose down
