from django.db import connection
from django.test import TestCase

from prescriptions.repository import prescription_repository


class TestRepository(TestCase):
    def setUp(self):
        self.cursor = connection.cursor()

    def tearDown(self):
        self.cursor.close()

    def test_add(self):
        data = {
            "physician_id": 1,
            "clinic_id": 2,
            "patient_id": 3,
            "text": "Dipirona 2x ao dia",
        }

        prescription_repository.add(data)

        self.cursor.execute("SELECT * from prescriptions_prescription")
        _, physician_id, clinic_id, patient_id, text = self.cursor.fetchone()
        self.assertEqual(data["physician_id"], physician_id)
        self.assertEqual(data["clinic_id"], clinic_id)
        self.assertEqual(data["patient_id"], patient_id)
        self.assertEqual(data["text"], text)
