import uuid

from django.test import TestCase

from prescriptions.models import Prescription
from prescriptions.services import create_metrics, create_prescription, save_metrics


class TestPrescriptionsServiceIntegration(TestCase):
    def setUp(self):
        self.prescription_data = {
            "clinic": {"id": 1},
            "physician": {"id": 1},
            "patient": {"id": 1},
            "text": "Dipirona 1x ao dia",
        }

    def uuid_is_valid(self, uuid_to_test, version=4):
        try:
            uuid_obj = uuid.UUID(uuid_to_test, version=version)
        except ValueError:
            return False
        return str(uuid_obj) == uuid_to_test

    def test_create_prescription(self):
        prescription = create_prescription(self.prescription_data)

        self.assertEqual(Prescription.objects.all().count(), 1)
        self.assertEqual(self.prescription_data["clinic"]["id"], prescription.clinic_id)
        self.assertEqual(
            self.prescription_data["physician"]["id"], prescription.physician_id
        )
        self.assertEqual(
            self.prescription_data["patient"]["id"], prescription.patient_id
        )
        self.assertEqual(self.prescription_data["text"], prescription.text)

    def test_create_metrics(self):
        prescription = create_prescription(self.prescription_data)
        metrics_data = create_metrics(prescription)

        self.assertTrue("clinic_id" in metrics_data)
        self.assertTrue("clinic_name" in metrics_data)
        self.assertTrue("physician_id" in metrics_data)
        self.assertTrue("physician_crm" in metrics_data)
        self.assertTrue("patient_id" in metrics_data)
        self.assertTrue("patient_name" in metrics_data)
        self.assertTrue("patient_email" in metrics_data)
        self.assertTrue("patient_phone" in metrics_data)
        self.assertEqual(metrics_data["prescription_id"], prescription.id)

    def test_save_metrics(self):
        prescription = create_prescription(self.prescription_data)
        metrics_data = create_metrics(prescription)

        id = save_metrics(metrics_data)
        self.assertTrue(self.uuid_is_valid(id))
