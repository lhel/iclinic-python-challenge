from django.db import connection
from django.db.utils import DataError, IntegrityError
from django.test import TestCase

from prescriptions.models import Prescription


class TestPrescriptionModel(TestCase):
    def setUp(self):
        self.cursor = connection.cursor()

    def tearDown(self):
        self.cursor.close()

    def create_prescription(
        self, physician_id=1, clinic_id=2, patient_id=3, text="Dipirona 1x ao dia"
    ):
        p = Prescription(
            physician_id=physician_id,
            clinic_id=clinic_id,
            patient_id=patient_id,
            text=text,
        )
        p.save()
        return p

    def test_create_prescription(self):
        p = Prescription(
            physician_id=1, clinic_id=2, patient_id=3, text="Dipirona 1x ao dia"
        )
        p.save()
        self.cursor.execute("SELECT * from prescriptions_prescription")
        _, physician_id, clinic_id, patient_id, text = self.cursor.fetchone()

        self.assertEqual(p.physician_id, physician_id)
        self.assertEqual(p.clinic_id, clinic_id)
        self.assertEqual(p.patient_id, patient_id)
        self.assertEqual(p.text, text)

    def test_create_prescription_integrity_error_physician_id_null(self):
        p = Prescription(clinic_id=2, patient_id=3, text="Dipirona 1x ao dia")
        with self.assertRaises(IntegrityError):
            p.save()

    def test_create_prescription_integrity_error_clinic_id_null(self):
        p = Prescription(physician_id=2, patient_id=3, text="Dipirona 1x ao dia")
        with self.assertRaises(IntegrityError):
            p.save()

    def test_create_prescription_integrity_error_patient_id_null(self):
        p = Prescription(physician_id=2, clinic_id=3, text="Dipirona 1x ao dia")
        with self.assertRaises(IntegrityError):
            p.save()

    def test_create_prescription_integrity_error_text_null(self):
        p = Prescription(physician_id=2, clinic_id=3, patient_id=1)
        with self.assertRaises(IntegrityError):
            p.save()

    def test_create_prescription_data_error_text_max_length(self):
        p = Prescription(physician_id=2, clinic_id=3, patient_id=1, text="x" * 141)
        with self.assertRaises(DataError):
            p.save()

    def test_update_prescription(self):
        self.create_prescription()
        expected_value = "Paracetamol 1x ao dia"
        Prescription.objects.filter(physician_id=1).update(text=expected_value)

        self.cursor.execute("SELECT * from prescriptions_prescription")
        _, physician_id, clinic_id, patient_id, text = self.cursor.fetchone()
        self.assertEqual(text, expected_value)

    def test_delete_prescription(self):
        p = self.create_prescription()
        p.delete()

        self.cursor.execute("SELECT * from prescriptions_prescription")
        self.assertEqual(self.cursor.fetchone(), None)
