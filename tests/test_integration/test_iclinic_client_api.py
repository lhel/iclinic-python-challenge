from uuid import UUID

from django.test import TestCase

from prescriptions.iclinic_client_api import exceptions
from prescriptions.iclinic_client_api.core import IClinicJsonAPI


class TestIClinicClientAPI(TestCase):
    def setUp(self):
        self.client_api = IClinicJsonAPI()

    def uuid_is_valid(self, uuid_to_test, version=4):
        try:
            uuid_obj = UUID(uuid_to_test, version=version)
        except ValueError:
            return False
        return str(uuid_obj) == uuid_to_test

    def test_get_physician(self):
        data = self.client_api.get_physician(1)

        self.assertTrue("id" in data)
        self.assertTrue("name" in data)
        self.assertTrue("crm" in data)

    def test_get_physician_not_found(self):
        with self.assertRaises(exceptions.PhysicianNotFound):
            self.client_api.get_physician(99999999999)

    def test_get_physician_service_not_available(self):
        with self.settings(
            PHYSICIANS_RETRY=0,
            PHYSICIANS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            PHYSICIANS_ENDPOINT="http://10.255.255.1/physicians/%d",
        ):
            with self.assertRaises(exceptions.PhysiciansServiceNotAvailable):
                self.client_api.get_physician(1)

    def test_get_clinic(self):
        data = self.client_api.get_clinic(1)

        self.assertTrue("id" in data)
        self.assertTrue("name" in data)

    def test_get_clinic_not_found(self):
        data = self.client_api.get_clinic(99999999999)

        self.assertEqual(data, {})

    def test_get_clinic_service_not_available(self):

        with self.settings(
            CLINICS_RETRY=0,
            CLINICS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            CLINICS_ENDPOINT="http://10.255.255.1/clinics/%d",
        ):
            data = self.client_api.get_clinic(99999999999)
        self.assertEqual(data, {})

    def test_get_patient(self):
        data = self.client_api.get_patient(1)

        self.assertTrue("id" in data)
        self.assertTrue("name" in data)
        self.assertTrue("email" in data)
        self.assertTrue("phone" in data)

    def test_get_patient_not_found(self):
        with self.assertRaises(exceptions.PatientNotFound):
            self.client_api.get_patient(99999999999)

    def test_get_patient_service_not_available(self):
        with self.settings(
            PATIENTS_RETRY=0,
            PATIENTS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            PATIENTS_ENDPOINT="http://10.255.255.1/patients/%d",
        ):
            with self.assertRaises(exceptions.PatientsServiceNotAvailable):
                self.client_api.get_patient(1)

    def test_create_metric(self):
        data = {
            "clinic_id": 1,
            "clinic_name": "Clínica A",
            "physician_id": 1,
            "physician_name": "José",
            "physician_crm": "SP293893",
            "patient_id": 1,
            "patient_name": "Mario",
            "patient_email": "rodrigo@gmail.com",
            "patient_phone": "(16)998765625",
            "prescription_id": 1,
        }
        result = self.client_api.create_metrics(data)

        self.assertTrue(self.uuid_is_valid(result))

    def test_create_metrics_service_not_available(self):
        data = {
            "clinic_id": 1,
            "clinic_name": "Clínica A",
            "physician_id": 1,
            "physician_name": "José",
            "physician_crm": "SP293893",
            "patient_id": 1,
            "patient_name": "Mario",
            "patient_email": "rodrigo@gmail.com",
            "patient_phone": "(16)998765625",
            "prescription_id": 1,
        }
        with self.settings(
            METRICS_RETRY=0,
            METRICS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            METRICS_ENDPOINT="http://10.255.255.1/metrics/%d",
        ):
            with self.assertRaises(exceptions.MetricsServiceNotAvailable):
                self.client_api.create_metrics(data)
