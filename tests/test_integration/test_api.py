from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from prescriptions.models import Prescription


class TestPrescriptionsAPI(APITestCase):
    def setUp(self):
        self.payload = {
            "clinic": {"id": 1},
            "physician": {"id": 1},
            "patient": {"id": 1},
            "text": "Dipirona 1x ao dia",
        }
        self.endpoint = reverse("prescriptions")

    def test_create_prescription(self):
        response = self.client.post(self.endpoint, self.payload, format="json")
        data = response.data["data"]

        self.assertEqual(Prescription.objects.all().count(), 1)
        prescription = Prescription.objects.all()[0]

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(data["id"], prescription.id)
        self.assertEqual(data["clinic"]["id"], prescription.clinic_id)
        self.assertEqual(data["physician"]["id"], prescription.physician_id)
        self.assertEqual(data["patient"]["id"], prescription.patient_id)
        self.assertEqual(data["text"], prescription.text)
        self.assertTrue("metric" in data)
        self.assertTrue("id" in data["metric"])

    def test_create_prescription_malformed_request(self):
        del self.payload["physician"]
        response = self.client.post(self.endpoint, self.payload, format="json")
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(data["code"], "01")
        self.assertEqual(data["message"], "malformed request")

    def test_create_prescription_physician_not_found(self):
        self.payload["physician"]["id"] = 99999999
        response = self.client.post(self.endpoint, self.payload, format="json")
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(data["code"], "02")
        self.assertEqual(data["message"], "physician not found")

    def test_create_prescription_patient_not_found(self):
        self.payload["patient"]["id"] = 99999999
        response = self.client.post(self.endpoint, self.payload, format="json")
        data = response.data
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(data["code"], "03")
        self.assertEqual(str(data["message"]), "patient not found")

    def test_create_prescription_metrics_service_not_available(self):
        with self.settings(
            METRICS_RETRY=0,
            METRICS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            METRICS_ENDPOINT="http://10.255.255.1/metricts/%d",
        ):
            response = self.client.post(self.endpoint, self.payload, format="json")
            data = response.data
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            self.assertEqual(data["code"], "04")
            self.assertEqual(data["message"], "metrics service not available")

    def test_create_prescription_physicians_service_not_available(self):
        with self.settings(
            PHYSICIANS_RETRY=0,
            PHYSICIANS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            PHYSICIANS_ENDPOINT="http://10.255.255.1/physicians/%d",
        ):
            self.payload["physician"]["id"] = 2
            response = self.client.post(self.endpoint, self.payload, format="json")
            data = response.data
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            self.assertEqual(data["code"], "05")
            self.assertEqual(data["message"], "physicians service not available")

    def test_create_prescription_patients_service_not_available(self):
        with self.settings(
            PATIENTS_RETRY=0,
            PATIENTS_TIMEOUT=1,
            # See https://stackoverflow.com/questions/100841/
            PATIENTS_ENDPOINT="http://10.255.255.1/patients/%d",
        ):
            self.payload["patient"]["id"] = 2
            response = self.client.post(self.endpoint, self.payload, format="json")
            data = response.data
            self.assertEqual(response.status_code, status.HTTP_503_SERVICE_UNAVAILABLE)
            self.assertEqual(data["code"], "06")
            self.assertEqual(data["message"], "patients service not available")
