from django.test import TestCase

from prescriptions.serializers import PrescriptionSerializer


class TestMetricSerializer(TestCase):
    def test_metric_serializer(self):
        data = {
            "clinic": {"id": 1},
            "physician": {"id": 1},
            "patient": {"id": 1},
            "text": "Dipirona 1x ao dia",
        }

        self.assertTrue(PrescriptionSerializer(data=data).is_valid())

    def test_metric_serializer_validation_error(self):
        data = {
            "clinic": {"invalid_field": 1},
            "physician": {"id": 1},
            "patient": {"id": 1},
            "text": "Dipirona 1x ao dia",
        }

        self.assertFalse(PrescriptionSerializer(data=data).is_valid())
