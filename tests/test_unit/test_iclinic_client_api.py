from unittest.mock import Mock, patch

import httpx
from django.test import TestCase

from prescriptions.iclinic_client_api import exceptions
from prescriptions.iclinic_client_api.core import IClinicJsonAPI


class TestIClinicAPI(TestCase):
    def setUp(self):
        self.client_api = IClinicJsonAPI()

    @patch.object(httpx.Client, "get")
    def test_get_physician(self, mock_get):
        expected_value = {"id": 47, "name": "Sr. Vicente Moreira", "crm": "17962384"}
        mock_get.return_value = Mock()
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = expected_value
        data = self.client_api.get_physician(47)

        self.assertEqual(data, expected_value)

    @patch.object(httpx.Client, "get")
    def test_get_physician_not_found(self, mock_get):
        response = Mock()
        response.status_code = 404
        exc = httpx.HTTPStatusError("Not Found", request=Mock(), response=response)
        mock_get.side_effect = exc
        with self.assertRaises(exceptions.PhysicianNotFound):
            self.client_api.get_physician(47)

    @patch.object(httpx.Client, "get")
    def test_get_physician_service_not_available(self, mock_get):
        exc = httpx.TimeoutException("Request Timeout", request=Mock())
        mock_get.side_effect = exc
        with self.assertRaises(exceptions.PhysiciansServiceNotAvailable):
            self.client_api.get_physician(47)

    @patch.object(httpx.Client, "get")
    def test_get_clinic(self, mock_get):
        expected_value = {"id": 1, "name": "Lopes"}
        mock_get.return_value = Mock()
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = expected_value
        data = self.client_api.get_clinic(47)

        self.assertEqual(data, expected_value)

    @patch.object(httpx.Client, "get")
    def test_get_clinic_not_found(self, mock_get):
        response = Mock()
        response.status_code = 404
        exc = httpx.HTTPStatusError("Not Found", request=Mock(), response=response)
        mock_get.side_effect = exc
        result = self.client_api.get_clinic(47)
        self.assertEqual(result, {})

    @patch.object(httpx.Client, "get")
    def test_get_clinic_service_not_available(self, mock_get):
        exc = httpx.TimeoutException("Request Timeout", request=Mock())
        mock_get.side_effect = exc
        result = self.client_api.get_clinic(47)
        self.assertEqual(result, {})

    @patch.object(httpx.Client, "get")
    def test_get_patient(self, mock_get):
        expected_value = {
            "id": 1,
            "name": "Pedro Henrique Aragão",
            "email": "anacampos@nogueira.net",
            "phone": "(051) 2502-3645",
        }
        mock_get.return_value = Mock()
        mock_get.return_value.status_code = 200
        mock_get.return_value.json.return_value = expected_value
        data = self.client_api.get_patient(1)

        self.assertEqual(data, expected_value)

    @patch.object(httpx.Client, "get")
    def test_get_patient_not_found(self, mock_get):
        response = Mock()
        response.status_code = 404
        exc = httpx.HTTPStatusError("Not Found", request=Mock(), response=response)
        mock_get.side_effect = exc
        with self.assertRaises(exceptions.PatientNotFound):
            self.client_api.get_patient(1)

    @patch.object(httpx.Client, "get")
    def test_get_patient_service_not_available(self, mock_get):
        exc = httpx.TimeoutException("Request Timeout", request=Mock())
        mock_get.side_effect = exc
        with self.assertRaises(exceptions.PatientsServiceNotAvailable):
            self.client_api.get_patient(1)

    @patch.object(httpx.Client, "post")
    def test_create_metric(self, mock_post):
        data = {
            "clinic_id": 1,
            "clinic_name": "Clínica A",
            "physician_id": 1,
            "physician_name": "José",
            "physician_crm": "SP293893",
            "patient_id": 1,
            "patient_name": "Mario",
            "patient_email": "rodrigo@gmail.com",
            "patient_phone": "(16)998765625",
            "prescription_id": 1,
        }
        expected_value = {"id": 1, **data}
        mock_post.return_value = Mock()
        mock_post.return_value.status_code = 201
        mock_post.return_value.json.return_value = expected_value
        result = self.client_api.create_metrics(data)

        self.assertEqual(result, expected_value["id"])

    @patch.object(httpx.Client, "post")
    def test_create_metrics_service_not_available(self, mock_post):
        data = {
            "clinic_id": 1,
            "clinic_name": "Clínica A",
            "physician_id": 1,
            "physician_name": "José",
            "physician_crm": "SP293893",
            "patient_id": 1,
            "patient_name": "Mario",
            "patient_email": "rodrigo@gmail.com",
            "patient_phone": "(16)998765625",
            "prescription_id": 1,
        }
        exc = httpx.TimeoutException("Request Timeout", request=Mock())
        mock_post.side_effect = exc
        with self.assertRaises(exceptions.MetricsServiceNotAvailable):
            self.client_api.create_metrics(data)
