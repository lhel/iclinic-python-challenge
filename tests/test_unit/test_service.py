import uuid
from typing import Dict, List

from django.test import TestCase

from prescriptions.models import Prescription
from prescriptions.services import create_metrics, create_prescription, save_metrics


class FakeRepository:
    def __init__(self):
        self.prescriptions = []

    def add(self, data: Dict):
        p = Prescription(**data)
        self.prescriptions.append(data)
        return p

    def __len__(self):
        return len(self.prescriptions)

    def __getitem__(self, pos):
        return self.prescriptions[pos]


class FakeIClinicClient:

    _physicians: List[Dict] = [
        {"id": 1, "name": "Evelyn da Rosa", "crm": "75423070"},
        {"id": 2, "name": "Juan das Neves", "crm": "45269325"},
        {"id": 3, "name": "Mario Andrade", "crm": "45269326"},
    ]
    _clinics: List[Dict] = [
        {"id": 1, "name": "Lopes"},
        {"id": 2, "name": "Andrade"},
        {"id": 3, "name": "Carvalho"},
    ]
    _patients: List[Dict] = [
        {
            "id": 1,
            "name": "Pedro Henrique Aragão",
            "email": "anacampos@nogueira.net",
            "phone": "(051) 2502-3645",
        },
        {
            "id": 2,
            "name": "Carlos Carvalho",
            "email": "carlos.carvalho@gmail.net",
            "phone": "(051) 2502-3645",
        },
        {
            "id": 3,
            "name": "Rafaela Moura",
            "email": "rafaela.moura@gmail.net",
            "phone": "(083) 3235-3645",
        },
    ]
    _metrics: List[Dict] = []

    def get_physician(self, id: int) -> Dict:
        return next(p for p in self._physicians if p["id"] == id)

    def get_clinic(self, id: int) -> Dict:
        return next(p for p in self._clinics if p["id"] == id)

    def get_patient(self, id: int) -> Dict:
        return next(p for p in self._patients if p["id"] == id)

    def create_metrics(self, metrics_data: Dict):
        data = {"id": uuid.uuid4(), **metrics_data}
        self._metrics.append(data)
        return data["id"]


class TestPrescriptionsService(TestCase):
    def setUp(self):
        self.fake_repository = FakeRepository()
        self.fake_client_api = FakeIClinicClient()
        self.prescription_data = {
            "clinic": {"id": 1},
            "physician": {"id": 1},
            "patient": {"id": 1},
            "text": "Dipirona 1x ao dia",
        }

    def test_save_prescription(self):

        prescription = create_prescription(self.prescription_data, self.fake_repository)

        self.assertEqual(len(self.fake_repository), 1)
        self.assertEqual(self.prescription_data["clinic"]["id"], prescription.clinic_id)
        self.assertEqual(
            self.prescription_data["physician"]["id"], prescription.physician_id
        )
        self.assertEqual(
            self.prescription_data["patient"]["id"], prescription.patient_id
        )
        self.assertEqual(self.prescription_data["text"], prescription.text)

    def test_create_metrics(self):
        prescription = create_prescription(self.prescription_data, self.fake_repository)
        metrics_data = create_metrics(prescription, self.fake_client_api)

        self.assertEqual(metrics_data["clinic_id"], 1)
        self.assertEqual(metrics_data["clinic_name"], "Lopes")
        self.assertEqual(metrics_data["physician_id"], 1)
        self.assertEqual(metrics_data["physician_crm"], "75423070")
        self.assertEqual(metrics_data["patient_id"], 1)
        self.assertEqual(metrics_data["patient_name"], "Pedro Henrique Aragão")
        self.assertEqual(metrics_data["patient_email"], "anacampos@nogueira.net")
        self.assertEqual(metrics_data["patient_phone"], "(051) 2502-3645")
        self.assertEqual(metrics_data["prescription_id"], prescription.id)

    def test_save_metrics(self):
        prescription = create_prescription(self.prescription_data, self.fake_repository)
        metrics_data = create_metrics(prescription, self.fake_client_api)

        id = save_metrics(metrics_data, self.fake_client_api)
        self.assertEqual(len(self.fake_client_api._metrics), 1)
        self.assertEqual(self.fake_client_api._metrics[0]["id"], id)
