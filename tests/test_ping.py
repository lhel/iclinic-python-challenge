from django.urls import reverse
from rest_framework.test import APITestCase


class TestPingPong(APITestCase):
    def test_ping(self):
        url = reverse("ping")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["message"], "pong")
        self.assertEqual(response.data["status"], "success")
