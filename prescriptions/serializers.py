from rest_framework import serializers


class ClinicField(serializers.Serializer):
    id = serializers.IntegerField()


class PhysicianField(serializers.Serializer):
    id = serializers.IntegerField()


class PatientField(serializers.Serializer):
    id = serializers.IntegerField()


class PrescriptionSerializer(serializers.Serializer):
    clinic = ClinicField()
    physician = PhysicianField()
    patient = PatientField()
    text = serializers.CharField()
