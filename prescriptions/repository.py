from typing import Dict, Protocol, Type

from django.db import models

from prescriptions.models import Prescription


class AbstractRepository(Protocol):
    def add(self, data: Dict) -> models.Model:
        ...


class Repository:
    def __init__(self, model_class: Type[models.Model]):
        self.model = model_class

    def add(self, data: Dict) -> models.Model:
        model_instance = self.model(**data)
        model_instance.save()
        return model_instance


prescription_repository: AbstractRepository = Repository(Prescription)
