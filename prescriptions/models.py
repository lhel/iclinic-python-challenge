from django.db import models


class Prescription(models.Model):
    physician_id = models.IntegerField()
    clinic_id = models.IntegerField()
    patient_id = models.IntegerField()
    text = models.CharField(max_length=140, default=None)
