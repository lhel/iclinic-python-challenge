from typing import Dict, Protocol

import httpx
from cachetools import TTLCache, cached
from django.conf import settings
from rest_framework import serializers

from . import exceptions
from .serializers import ClinicSerializer, PatientSerializer, PhysicianSerializer


class IClinicAPI(Protocol):

    """Interface to calls to IClinic API"""

    def get_physician(self, id: int) -> Dict:
        ...

    def get_clinic(self, id: int):
        ...

    def get_patient(self, id: int):
        ...

    def create_metrics(self, prescription_data: Dict):
        ...


class IClinicJsonAPI:

    """Implementation for IClinic json api calls."""

    _token_type = "Bearer"

    def _check_errors(self, response):
        if 400 <= response.status_code <= 514:
            response.raise_for_status()

    def _get(self, url, token, timeout, retries) -> Dict:
        headers = {
            "Authorization": f"{self._token_type} {token}",
            "Content-Type": "application/json",
        }
        transport = httpx.HTTPTransport(retries=retries)
        with httpx.Client(transport=transport) as client:
            response = client.get(url, headers=headers, timeout=timeout)
        self._check_errors(response)
        return response.json()

    def _post(self, url, token, timeout, retries, data) -> Dict:
        headers = {
            "Authorization": f"{self._token_type} {token}",
            "Content-Type": "application/json",
        }
        transport = httpx.HTTPTransport(retries=retries)
        with httpx.Client(transport=transport) as client:
            response = client.post(url, json=data, headers=headers, timeout=timeout)
        self._check_errors(response)
        return response.json()

    @cached(cache=TTLCache(maxsize=32, ttl=settings.PHYSICIANS_TTL))
    def get_physician(self, id: int) -> Dict:
        try:
            data = self._get(
                settings.PHYSICIANS_ENDPOINT % id,
                settings.PHYSICIANS_AUTH_TOKEN,
                settings.PHYSICIANS_TIMEOUT,
                settings.PHYSICIANS_RETRY,
            )
        except httpx.HTTPStatusError as exc:
            if exc.response.status_code == httpx.codes.not_found:
                raise exceptions.PhysicianNotFound
            else:
                raise exc
        except httpx.TimeoutException:
            raise exceptions.PhysiciansServiceNotAvailable

        serializer = PhysicianSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data

    @cached(cache=TTLCache(maxsize=32, ttl=settings.CLINICS_TTL))
    def get_clinic(self, id: int):
        try:
            data = self._get(
                settings.CLINICS_ENDPOINT % id,
                settings.CLINICS_AUTH_TOKEN,
                settings.CLINICS_TIMEOUT,
                settings.CLINICS_RETRY,
            )
        except (httpx.HTTPStatusError, httpx.TimeoutException):
            return {}
        serializer = ClinicSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data

    @cached(cache=TTLCache(maxsize=32, ttl=settings.PATIENTS_TTL))
    def get_patient(self, id: int):
        try:
            data = self._get(
                settings.PATIENTS_ENDPOINT % id,
                settings.PATIENTS_AUTH_TOKEN,
                settings.PATIENTS_TIMEOUT,
                settings.PATIENTS_RETRY,
            )
        except httpx.HTTPStatusError as exc:
            if exc.response.status_code == httpx.codes.not_found:
                raise exceptions.PatientNotFound
            else:
                raise exc
        except httpx.TimeoutException:
            raise exceptions.PatientsServiceNotAvailable

        serializer = PatientSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data

    def create_metrics(self, metrics_data: Dict):
        try:
            data = self._post(
                settings.METRICS_ENDPOINT,
                settings.METRICS_AUTH_TOKEN,
                settings.METRICS_TIMEOUT,
                settings.METRICS_RETRY,
                metrics_data,
            )
        except httpx.TimeoutException:
            raise exceptions.MetricsServiceNotAvailable

        if "id" in data:
            return data["id"]
        else:
            raise serializers.ValidationError(code=400)


iclinic_json_api: IClinicAPI = IClinicJsonAPI()
