from rest_framework import serializers


class PhysicianSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    crm = serializers.CharField(min_length=7, max_length=13)


class ClinicSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()


class PatientSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    email = serializers.EmailField()
    phone = serializers.CharField()
