from rest_framework.exceptions import APIException


class PhysicianNotFound(APIException):
    status_code = 404
    default_code = "02"
    default_detail = "physician not found"


class PatientNotFound(APIException):
    status_code = 404
    default_code = "03"
    default_detail = "patient not found"


class MetricsServiceNotAvailable(APIException):
    status_code = 503
    default_code = "04"
    default_detail = "metrics service not available"


class PhysiciansServiceNotAvailable(APIException):
    status_code = 503
    default_code = "05"
    default_detail = "physicians service not available"


class PatientsServiceNotAvailable(APIException):
    status_code = 503
    default_code = "06"
    default_detail = "patients service not available"
