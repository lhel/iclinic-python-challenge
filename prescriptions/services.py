import uuid
from typing import Dict

from prescriptions.iclinic_client_api.core import IClinicAPI, iclinic_json_api
from prescriptions.models import Prescription
from prescriptions.repository import AbstractRepository, prescription_repository


def create_prescription(
    prescription_data: Dict, repo: AbstractRepository = prescription_repository
) -> Prescription:
    return repo.add(
        {
            "physician_id": prescription_data["physician"]["id"],
            "clinic_id": prescription_data["clinic"]["id"],
            "patient_id": prescription_data["patient"]["id"],
            "text": prescription_data["text"],
        }
    )


def create_metrics(
    prescription: Prescription, client_api: IClinicAPI = iclinic_json_api
) -> Dict:
    metrics_data = {}

    physician_data = client_api.get_physician(prescription.physician_id)
    metrics_data.update(
        {
            "physician_id": physician_data["id"],
            "physician_name": physician_data["name"],
            "physician_crm": physician_data["crm"],
        }
    )

    clinic_data = client_api.get_clinic(prescription.clinic_id)
    if len(clinic_data) > 0:
        metrics_data.update(
            {"clinic_id": clinic_data["id"], "clinic_name": clinic_data["name"]}
        )

    patient_data = client_api.get_patient(prescription.patient_id)
    metrics_data.update(
        {
            "patient_id": patient_data["id"],
            "patient_name": patient_data["name"],
            "patient_email": patient_data["email"],
            "patient_phone": patient_data["phone"],
        }
    )
    metrics_data.update({"prescription_id": prescription.id})
    return metrics_data


def save_metrics(
    metrics_data: Dict, client_api: IClinicAPI = iclinic_json_api
) -> uuid.UUID:
    return client_api.create_metrics(metrics_data)
