from django.db import transaction
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from prescriptions import services
from prescriptions.serializers import PrescriptionSerializer


@swagger_auto_schema(
    operation_id="create_prescription",
    operation_description="Create a prescription.",
    methods=["post"],
    request_body=PrescriptionSerializer,
)
@api_view(["POST"])
@transaction.atomic
def create_prescription(request):
    serializer = PrescriptionSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    prescription = services.create_prescription(serializer.validated_data)
    metrics_data = services.create_metrics(prescription)
    metrics_id = services.save_metrics(metrics_data)

    result = {"metric": {"id": metrics_id}, "id": prescription.id}
    result.update(serializer.validated_data)
    return Response({"data": result}, status.HTTP_201_CREATED)
