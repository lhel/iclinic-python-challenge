from functools import singledispatch

from rest_framework import exceptions
from rest_framework.views import exception_handler

from prescriptions.iclinic_client_api import exceptions as api_exc


@singledispatch
def handle_error(exc, context):
    return exception_handler(exc, context)


@handle_error.register(exceptions.ParseError)
@handle_error.register(exceptions.ValidationError)
def _(exc, context):
    response = exception_handler(exc, context)
    if response is not None:
        response.data = {"code": "01", "message": "malformed request"}

    return response


@handle_error.register(api_exc.PhysiciansServiceNotAvailable)
@handle_error.register(api_exc.PatientsServiceNotAvailable)
@handle_error.register(api_exc.MetricsServiceNotAvailable)
@handle_error.register(api_exc.PatientNotFound)
@handle_error.register(api_exc.PhysicianNotFound)
def _(exc, context):
    response = exception_handler(exc, context)
    if response is not None:
        response.data = exc.get_full_details()

    return response
