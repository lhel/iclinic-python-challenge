# IClinic Python Challenge

[![pipeline status](https://gitlab.com/lhel/iclinic-python-challenge/badges/master/pipeline.svg)](https://gitlab.com/lhel/iclinic-python-challenge/commits/master)

This repository contains the source code for a REST API to medical prescriptions as part of the tecnical challenge from IClinic.

- [Application Deploy URL](https://nameless-journey-60623.herokuapp.com/)
- [API Documentation](https://app.swaggerhub.com/apis/luheeslo/prescriptions-api/v1)

## Prerequisites

For run this project, install [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).

## Development

To start developing follow these steps:

1. Clone this repository:

        git clone https://github.com/luheeslo/instagram_clone

2. Change the file `.env.example` to `.env`

3. Configure the [dependent services tokens](https://github.com/iclinic/iclinic-python-challenge#servi%C3%A7os-dependentes) in `.env` file. 
4. Run `make build` to build the image.

5. Run `make runserver` to run the containers.

6. Run `http://localhost:8000/ping` to check if the API is online.

While running, you can do one of the following actions:

- `make makemigrations` and `make migrate` to migrations.
- `make shell` to run Python shell.
- `make test` to run tests and use PATH_DIR optional variable to choose the directory test(ex.: `make test PATH_DIR=tests/test_integration/`).
- `make logs` to show logs of API and database.
- `make flake8` for check code style and quality .
- `make black` for apply code formatting with best practices.
- `make test_coverage` and `make report_html` for measuring code coverage and generate report.

For shutdown API and database, run `make down`

### Install packages

For install new packages follow these steps:

- Install [Pipenv](https://pipenv.pypa.io/en/latest/).
- Run `pipenv install package` to install the `package` library.
- Run `make build` to create the new image with `package` installed.
